# Text to Graph
Some useless code.

## Requirements
* [Graphviz](https://www.graphviz.org/)
* [graphviz](https://github.com/xflr6/graphviz) python library

## Usage
```python
chorus = """I'm Blue da ba dee da ba dye da ba dee da ba dye
Da ba dee da ba dye da ba dee da ba dye
Da ba dee da ba dye da ba dee da ba dye
Da ba dee da ba dye
I'm Blue da ba dee da ba dye da ba dee da ba dye
Da ba dee da ba dye da ba dee da ba dye
Da ba dee da ba dye da ba dee da ba dye
Da ba dee da ba dye
"""

draw_from_text(chorus, 'chorus')
```
Two files will appear:
* `chorus.gv` - definition of graph in the [DOT](https://www.graphviz.org/doc/info/lang.html) language
* `chorus.gv.png` - generated graph