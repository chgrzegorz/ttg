import itertools
import string

from graphviz import Source

def draw_from_text(text, title):
    dot = make_graph(text, title)
    Source(dot, format='png').render(f'{title}.gv')

def make_graph(text, title):
    words = text.lower().split()
    names = {word: label for word, label in zip(unique_everseen(words), name_generator())}
    labels_definition = '\n\t'.join(generate_labels(names))
    nodes_definition = '\n\t'.join(generate_nodes(words, names))
    graph_code = f'''digraph "{title}"{"{"}
    {labels_definition}
    {nodes_definition}\n{"}"}'''
    return graph_code

def name_generator():
    for i in range(1, 65):
        for name in itertools.product(string.ascii_uppercase, repeat=i):
            yield ''.join(name)

def generate_labels(names):
    return [f'{label} [label="{word}"]' for word, label in names.items()]

def generate_nodes(words, names):
    coded_words = [names[word] for word in words]
    mapping = {f'{w1} -> {w2}' for w1, w2 in zip(coded_words, coded_words[1:])}
    return sorted(mapping)  

def unique_everseen(iterable, key=None):
    """List unique elements, preserving order. Remember all elements ever seen."""
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in itertools.filterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield elements